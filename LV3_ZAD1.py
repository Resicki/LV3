import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

br=0
mtcylsv6=0
mtcylsv4=0
amcars=0
amcar100=0
mtcars = pd.read_csv('mtcars.csv')  

mtmax=mtcars.sort(['mpg'], ascending=[0])   
print("1. Automobili s najvećom potrošnjom su : \n", mtmax[:5])

mtcylmin=mtcars[mtcars.cyl==8].sort(['mpg'], ascending=[1]) 
print("2.  Automobili s najvećom potrošnjom s osam cilindara : \n", mtcylmin[:3])

mtcylsv6+=mtcars[mtcars.cyl==6].mpg 
print("\n3.  Automobili sa 6 cilindara imaju srednju potrošnju: ", sum(mtcylsv6)/len(mtcylsv6)) 

mtcylsv4+=mtcars[(mtcars.cyl==4) & (mtcars.wt>=2) & (mtcars.wt<=2.2)].mpg 
print("\n4.   Srednja potrošnja automobila s 4 cilindra mase između 2000 i 2200 lbs je: ", sum(mtcylsv4)/len(mtcylsv4))

amcars+=mtcars[mtcars.am==0].mpg 
print("\n5. Imamo", len(amcars), "automobila sa automatskih mjenjačem, a", len(mtcars)-len(amcars), "s ručnim mjenjačem.")

amcar100+=mtcars[(mtcars.am==0) & (mtcars.hp>=100)].mpg 
print("\n6. Imamo", len(amcar100), "automobila s automatskim mjenjačem, snage preko 100 KS.")

mtcars['kg'] = mtcars.wt*1000*0.45359237 
print("\n7. Mase automobila u kilogramima:\n", mtcars[['car','kg']])


